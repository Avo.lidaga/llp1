#include "client_manager/database/database.h"
#include "client_manager/table/row_builder/row_builder.h"
#include "client_manager/table/table_client.h"

static void insert(struct database *db, int n, char *table_name) {
    struct batch_builder batch = batch_builder_init(n);
    for (int i = 0; i < n; i++) {
        int integer = 228;
        float floating = 0.5f;
        int boolean = 1;
        char string[64] = "12345678123456781234567812345678";
        struct row_builder row = row_builder_init(4);
        row_builder_add(&row, column_int(integer));
        row_builder_add(&row, column_string(string));
        row_builder_add(&row, column_bool(!!boolean));
        row_builder_add(&row, column_float(floating));
        batch_builder_add(&batch, row);
    }
    enum table_status status = table_insert(db, table_name, batch);
    batch_builder_free(&batch);
}

void table_create(struct database *db, char *table_name) {
    struct table_builder *scheme_builder = table_builder_init(table_name);
    table_builder_add_column(scheme_builder, "int", DATA_TYPE_INT);
    table_builder_add_column(scheme_builder, "string", DATA_TYPE_STRING);
    table_builder_add_column(scheme_builder, "bool", DATA_TYPE_BOOL);
    table_builder_add_column(scheme_builder, "float", DATA_TYPE_FLOAT);
    create_table(db, scheme_builder);
    table_builder_free(&scheme_builder);
}

int main() {
    char *table_name = "test2";
    struct database *db = open_db("test.txt");
    if (db != NULL) {
//        table_create(db, table_name);
        insert(db, 1000, table_name);
        save_db(&db);
        return 0;
    }
    return -1;
}

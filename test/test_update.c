#include <time.h>
#include <stdio.h>
#include "client_manager/database/database.h"
#include "client_manager/table/condition/condition.h"
#include "client_manager/table/table_client.h"

void update(struct database *db) {
    struct where_condition *condition = where_condition_compare(
            COMPARE_EQ,
            operand_column("test", "bool"),
            operand_literal_bool(true)
    );
    struct query query = {
            .table = "test",
            .where = condition,
            .joins = NULL
    };

    struct updater_builder *updater = updater_builder_init();
    updater_builder_add(updater,
                        column_updater_of(
                                "bool",
                                column_bool(true)
                        )
    );
    updater_builder_add(updater,
                        column_updater_of(
                                "string",
                                column_string("updated")
                        )
    );

    clock_t begin = clock();
    table_update(db, query, updater);
    clock_t end = clock();
    double time_spent = (double) (end - begin) / CLOCKS_PER_SEC;
    printf("%f", time_spent);
    where_condition_free(condition);
    updater_builder_free(&updater);
}

int main() {
    struct database *db = open_db("test.txt");
    if (db != NULL) {
        update(db);
        save_db(&db);
        return 0;
    }
    return -1;
}

#include <stdio.h>
#include "client_manager/database/database.h"
#include "client_manager/table/selector/selector.h"
#include "client_manager/table/table_client.h"

void print_row(struct row row) {
    for (size_t i = 0; i < row.size; i++) {
        struct row_column column = row.columns[i];
        switch (column.type) {
            case DATA_TYPE_INT:
                printf("%d ", column.value.val_int);
                break;
            case DATA_TYPE_FLOAT:
                printf("%f ", column.value.val_float);
                break;
            case DATA_TYPE_STRING:
                printf("%s ", column.value.val_string);
                break;
            case DATA_TYPE_BOOL:
                printf("%d ", column.value.val_bool);
                break;
        }
    }
    printf(" \n");
}

void selecting(struct database *db) {
    struct query query = {.table = "test", .where = NULL, .joins = NULL};
    struct selector_builder *selector = selector_builder_init();
    selector_builder_add(selector, "test", "int");
    selector_builder_add(selector, "test", "string");
    selector_builder_add(selector, "test", "bool");
    selector_builder_add(selector, "test", "float");
    struct result_view *view = table_select(db, query, selector);
    if (view == NULL) {
        printf("Can't create select \n");
        return;
    }
    size_t count = 0;
    while (!result_view_is_empty(view)) {
        struct row row = result_view_get(view);
        print_row(row);
        row_free(row);
        result_view_next(view);
        count++;
    }
    result_view_free(&view);
    selector_builder_free(&selector);
    printf("%zu", count);
}

int main(void) {
    struct database *db = open_db("test.txt");
    if (db != NULL) {
        selecting(db);
        save_db(&db);
        return 0;
    }
    return -1;
}

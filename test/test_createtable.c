#include <assert.h>
#include <stdlib.h>
#include "client_manager/database/database.h"
#include "client_manager/table/table_builder/table_builder.h"
#include "client_manager/table/table_client.h"

void table_create(struct database *db, char *table_name) {
    struct table_builder *scheme_builder = table_builder_init(table_name);
    table_builder_add_column(scheme_builder, "int", DATA_TYPE_INT);
    table_builder_add_column(scheme_builder, "string", DATA_TYPE_STRING);
    table_builder_add_column(scheme_builder, "bool", DATA_TYPE_BOOL);
    table_builder_add_column(scheme_builder, "float", DATA_TYPE_FLOAT);
    enum table_status status = create_table(db, scheme_builder);
    table_builder_free(&scheme_builder);
}

int main(int argc, char *argv[]) {
    assert(argc == 3);
    enum database_init mode = atoi(argv[1]);
    char *table_name = argv[2];
    struct database *db = mode == DATABASE_CREATE ? create_db("test.txt") : open_db("test.txt");
    if (db != NULL) {
        table_create(db, table_name);
        save_db(&db);
        return 0;
    }
    return -1;
}


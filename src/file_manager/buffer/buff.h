#ifndef LLP_BUFF_H
#define LLP_BUFF_H

#include <stdint.h>
#include <stdbool.h>

struct buff {
    uint64_t size;
    char *data;
    uint64_t rcurs;
    uint64_t wcurs;
};

union u32 {
    int32_t i32;
    uint32_t ui32;
    float f32;
};

union u64 {
    int64_t i64;
    uint64_t ui64;
    double f64;
};

union u8 {
    int8_t i8;
    uint8_t ui8;
};
struct buff *buffer_init(uint64_t size);
void buffer_free(struct buff **buffer_pp);
struct buff *buffer_copy(const struct buff *buffer);
void buffer_reset(struct buff *buffer);
bool buffer_is_empty(const struct buff *buffer);
char *buffer_read_string(struct buff *buffer);
union u64 buffer_read_b64(struct buff *buffer);
union u32 buffer_read_b32(struct buff *buffer);
union u8 buffer_read_b8(struct buff *buffer);
void buffer_write_string(struct buff *buffer, const char *string);
void buffer_write_b64(struct buff *buffer, union u64 num);
void buffer_write_b32(struct buff *buffer, union u32 num);
void buffer_write_b8(struct buff *buffer, union u8 num);
struct buff *buffer_merge(struct buff *first_buffer, struct buff *second_buffer);

#endif

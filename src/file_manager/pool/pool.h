

#ifndef LLP_POOL_H
#define LLP_POOL_H

#include <stdbool.h>
#include <stdint.h>
#include "file_manager/buffer/buff.h"

#define POOL_SIZE  55
#define POOL_START 64
struct pool {
    struct database *db;
    struct page *page;
    struct page_chain_header *chains[POOL_SIZE];
};

struct pool_iter {
    struct pool *pool;
    int chain_idx;
    struct chain_iter *chain_it;
};

enum pool_status {
    POOL_SUCCESS = 0,
    POOL_CLEAR_ERROR,
    POOL_APPEND_ERROR,
    POOL_DELETE_ERROR,
    POOL_ITER_END,
    POOL_ITER_ERROR
};

int64_t pool_create(struct database *db);

struct pool *pool_init(struct database *db, int64_t pool_addr);

void pool_free(struct pool **pool);

enum pool_status pool_clear(struct pool *pool);

enum pool_status pool_append(struct pool *pool, struct buff *buffer);

struct pool_iter *pool_iterator(struct pool *pool);

void pool_iterator_free(struct pool_iter **iter);

bool pool_iterator_is_empty(struct pool_iter *it);

struct buff *pool_iterator_get(struct pool_iter *it);

enum pool_status pool_iterator_delete(struct pool_iter *it);

enum pool_status pool_iterator_next(struct pool_iter *it);

void pool_drop (struct pool **pool_p);

#endif //LLP_POOL_H

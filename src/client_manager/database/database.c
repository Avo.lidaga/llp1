#include <stdlib.h>
#include <memory.h>
#include <assert.h>
#include "database.h"
#include "file_manager/page/page_handler.h"
#include "file_manager/pool/pool.h"




struct database *open_db(char *file_name) {
    struct database *db = malloc(sizeof(struct database));
    if (db != NULL) {
        FILE *file = fopen(file_name, "rb+");
        if (file != NULL) {
            int8_t *page_data = malloc(PAGE_SIZE);
            if (page_data != NULL) {
                struct page first_page = {.data = page_data, .addr = 0, .header = {0, 0, 0}};
                enum page_status read_status = read_page(file, 0, &first_page);

                if (read_status == PAGE_SUCCESS) {
                    struct header *file_header = malloc(sizeof(struct header));
                    if (file_header != NULL) {
                        memcpy(file_header, first_page.data + sizeof(struct file_page_header), sizeof(struct header));
                        if (file_header->key == KEY) {
                            db->file = file;
                            db->header = file_header;
                            db->allocated_pages = NULL;
                            struct pool *pool = pool_init(db, db->header->table_metadata);
                            if (pool != NULL) {
                                db->table_pool = pool;
                                free(page_data);
                                return db;
                            }
                        }
                        free(file_header);
                    }
                }
                free(page_data);
            }
            fclose(file);
        }
        free(db);
    }
    return NULL;
}

//todo
struct database *create_db(char *file_name) {
    struct database *db = malloc(sizeof(struct database));
    if (db != NULL) {
        FILE *file = fopen(file_name, "wb+");
        if (file != NULL) {
            int8_t *page_data = malloc(PAGE_SIZE);
            if (page_data != NULL) {
                struct page first_page = {.data = page_data, .addr = 0, .header = {0, 0, 0}};
                enum page_status write_status = write_page(file, &first_page);
                if (write_status == FILE_SUCCESS) {
                    struct header *file_header = malloc(sizeof(struct header));
                    if (file_header != NULL) {
                        file_header->key = KEY;
                        file_header->empty_pages = 0;
                        db->header = file_header;
                        db->file = file;
                        db->allocated_pages = NULL;
                        db->table_pool = NULL;
                        int64_t addr = pool_create(db);
                        file_header->table_metadata = addr;
                        if (addr != 0) {
                            db->table_pool = pool_init(db, addr);
                            if (db->table_pool != NULL) {
                                free(page_data);
                                return db;
                            }
                        }
                        free(file_header);
                    }
                }
                free(page_data);
            }
            fclose(file);
        }
        free(db);
    }
    return NULL;
}

enum file_status save_db(struct database **db_p) {
    assert(db_p != NULL);
    struct database *db = *db_p;
    if (db == NULL) {
        return FAIL_WRITE_FILE;
    }
    int8_t *page_data = malloc(PAGE_SIZE);
    if (page_data != NULL) {
        struct page first_page = {
                .data = page_data,
                .addr = 0,
                .header = {
                        0,
                        0,
                        sizeof(struct file_page_header) + sizeof(struct header)
                }
        };
        memcpy(first_page.data + sizeof(struct file_page_header), db->header, sizeof(struct header));
        enum page_status write_status = write_page(db->file, &first_page);
        if (write_status != PAGE_SUCCESS) return FAIL_WRITE_FILE;
        pool_free(&db->table_pool);
        free(db->header);
        fclose(db->file);
        //free(db);
        free(db);
        *db_p = NULL;
        return FILE_SUCCESS;
    }
    return FAIL_WRITE_FILE;
}
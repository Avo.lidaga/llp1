#ifndef LLP_TABLE_CLIENT_H
#define LLP_TABLE_CLIENT_H

#include "client_manager/table/cursor/cursor.h"
#include "client_manager/table/join/join.h"
#include "client_manager/table/viewer/viewer.h"
#include "client_manager/table/selector/selector.h"
#include "client_manager/table/row_builder/row_builder.h"
#include "client_manager/table/table_builder/table_builder.h"
#include "client_manager/table/updater/updater.h"


struct query {
    char *table;
    struct join_condition_list *joins;
    struct where_condition *where;
};

struct buffer_list {
    struct buff *buffer;
    struct buffer_list *next;
};

enum table_status table_insert(struct database *db, char *name, struct batch_builder batch);
enum table_status create_table(struct database *db, struct table_builder *table_sample);
enum table_status delete_table(struct database *db, char *name);
struct result_view *table_select(struct database *db, struct query query, struct selector_builder *selector);
enum table_status table_delete(struct database *db, struct query query);
enum table_status table_update(struct database *db, struct query query, struct updater_builder *updater);

#endif

#ifndef LLP_JOIN_H
#define LLP_JOIN_H

#include "client_manager/table/table.h"
struct join_condition_list {
    struct column_description left;
    struct column_description right;
    struct join_condition_list *next;
};

struct join_condition_list *join_builder_init();
void join_builder_add(struct join_condition_list **list, struct column_description left, struct column_description right);
void join_builder_free(struct join_condition_list **list_p);

#endif

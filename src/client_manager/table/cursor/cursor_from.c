#include <assert.h>
#include <stdlib.h>
#include "cursor.h"
#include "file_manager/pool/pool.h"

static struct row cursor_get_row_from(struct cursor *cur) {
    assert(cur != NULL);
    if ((*cur).from.cached_row.columns != NULL) {
        return cur->from.cached_row;
    }
    struct buff *buffer = pool_iterator_get((*cur).from.it);
    if (buffer != NULL) {
        struct row row = row_deserialize((*cur).from.table->table_struct, buffer);
        buffer_free(&buffer);
        (*cur).from.cached_row = row;
        return row;
    }
    return (struct row) {.columns = NULL, .size = 0};
}

static void cursor_free_cached_row(struct cursor *cur) {
    assert(cur != NULL);
    if ((*cur).from.cached_row.columns == NULL) {
        return;
    }
    free((*cur).from.cached_row.columns);
    (*cur).from.cached_row.size = 0;
    (*cur).from.cached_row.columns = NULL;
}

static void cursor_free_from(struct cursor *cur) {
    assert(cur != NULL);
    cursor_free_cached_row(cur);
    table_free(&((*cur).from.table));
    pool_iterator_free(&((*cur).from.it));
    free(cur);
}

static bool cursor_is_empty_from(struct cursor *cur) {
    assert(cur != NULL);
    return pool_iterator_is_empty((*cur).from.it);
}

static void cursor_next_from(struct cursor *cur) {
    assert(cur != NULL);
    if (cursor_is_empty(cur)) {
        return;
    }
    cursor_free_cached_row(cur);
    pool_iterator_next((*cur).from.it);
}

static void cursor_restart_from(struct cursor *cur) {
    assert(cur != NULL);
    pool_iterator_free(&(cur->from.it));
    (*cur).from.it = pool_iterator((*cur).from.table->table_pool);
    cursor_free_cached_row(cur);
}

static struct row_column cursor_get_from(struct cursor *cur, size_t table_idx, size_t column_idx) {
    assert(cur != NULL);
    if ((*cur).from.table_idx == table_idx) {
        struct row row = cursor_get_row_from(cur);
        if (row.columns != NULL) {
            return row.columns[column_idx];
        }
    }
    return (struct row_column) {.type = 0, .value = 0};
}

static void cursor_delete_from(struct cursor *cur, size_t table_idx) {
    assert(cur != NULL);
    if ((*cur).from.table_idx == table_idx) {
        cursor_free_cached_row(cur);
        pool_iterator_delete((*cur).from.it);
    }
}

static void cursor_update_from(struct cursor *cur, size_t table_idx, struct updater_builder *updater) {
    if (cur->from.table_idx == table_idx) {
        struct row row = cursor_get_row_from(cur);
        struct row updated_row = updater_builder_update(updater, row);
        struct buff *serialized = row_serialize(updated_row);
        cursor_free_cached_row(cur);
        pool_iterator_delete((*cur).from.it);
        pool_append(cur->from.table->table_pool, serialized);
        row_free(updated_row);
        buffer_free(&serialized);
    }
}

struct cursor *cursor_init_from(struct table *table, size_t table_idx) {
    assert(table != NULL);
    struct cursor *cur = malloc(sizeof(struct cursor));
    if (cur != NULL) {
        (*cur).type = CURSOR_FROM;
        (*cur).from.table_idx = table_idx;
        (*cur).from.table = table;
        (*cur).from.it = pool_iterator(table->table_pool);
        (*cur).from.cached_row = (struct row) {0};
    }
    (*cur).free = cursor_free_from;
    (*cur).is_empty = cursor_is_empty_from;
    (*cur).next = cursor_next_from;
    (*cur).restart = cursor_restart_from;
    (*cur).get = cursor_get_from;
    (*cur).delete = cursor_delete_from;
    (*cur).update = cursor_update_from;
    return cur;
}



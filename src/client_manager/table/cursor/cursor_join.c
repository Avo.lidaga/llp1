#include <assert.h>
#include <stdlib.h>
#include "cursor.h"
#include "client_manager/table/condition/condition.h"

static bool join_condition_check(struct join_condition_list *condition, struct cursor *cur) {
    assert(condition->right.type == COLUMN_DESC_INDEX && condition->left.type == COLUMN_DESC_INDEX);
    struct row_column right_column = cursor_get(cur->join.right, condition->right.index.table_idx,
                                                condition->right.index.column_idx);
    struct row_column left_column = cursor_get(cur->join.left, condition->left.index.table_idx,
                                               condition->left.index.column_idx);
    return columns_equals(right_column, left_column);
}

static void cursor_free_join(struct cursor *cur) {
    cursor_free(&(cur->join.right));
    cursor_free(&(cur->join.left));
    free(cur);
}

static bool cursor_is_empty_join(struct cursor *cur) {
    return cursor_is_empty(cur->join.left) || cursor_is_empty(cur->join.right);
}

static void cursor_next_join(struct cursor *cur) {
    if (cursor_is_empty(cur)) {
        return;
    }
    cursor_next(cur->join.right);
    if (cursor_is_empty(cur->join.right)) {
        cursor_next(cur->join.left);
        cursor_restart(cur->join.right);
    }
    while (!cursor_is_empty(cur->join.left) && !cursor_is_empty(cur->join.right)) {
        if (join_condition_check(&(cur->join.condition), cur)) {
            break;
        }
        cursor_next(cur->join.right);
        if (cursor_is_empty(cur->join.right)) {
            cursor_next(cur->join.left);
            cursor_restart(cur->join.right);
        }
    }
}

static void cursor_restart_join(struct cursor *cur) {
    cursor_restart(cur->join.left);
    cursor_restart(cur->join.right);
    if (!join_condition_check(&(cur->join.condition), cur)) {
        cursor_next(cur);
    }
}

//Todo cursor use in join
static struct row_column cursor_get_join(struct cursor *cur, size_t table_idx, size_t column_idx) {
    struct row_column left_column = cursor_get(cur->join.left, table_idx, column_idx);
    struct row_column right_column = cursor_get(cur->join.right, table_idx, column_idx);
    if (left_column.type != 0) {
        assert(right_column.type == 0);
        return left_column;
    } else {
        assert(right_column.type != 0);
        assert(left_column.type == 0);
        return right_column;
    }
}

static void cursor_delete_join(struct cursor *cur, size_t table_idx) {
    cursor_delete(cur->join.left, table_idx);
    cursor_delete(cur->join.right, table_idx);
}

static void cursor_update_join(struct cursor *cur, size_t table_idx, struct updater_builder *updater) {
    cursor_update(cur->join.left, table_idx, updater);
    cursor_update(cur->join.right, table_idx, updater);
}

struct cursor *cursor_init_join(struct cursor *left, struct cursor *right, struct join_condition_list condition) {
    assert(left != NULL && right != NULL &&
           condition.right.type == COLUMN_DESC_INDEX &&
           condition.left.type == COLUMN_DESC_INDEX);

    struct cursor *cur = malloc(sizeof(struct cursor));
    if (cur != NULL) {
        cur->type = CURSOR_JOIN;
        cur->join.condition = condition;
        cur->join.right = right;
        cur->join.left = left;
        cur->free = cursor_free_join;
        cur->is_empty = cursor_is_empty_join;
        cur->next = cursor_next_join;
        cur->restart = cursor_restart_join;
        cur->get = cursor_get_join;
        cur->delete = cursor_delete_join;
        cur->update = cursor_update_join;
        if (!cursor_is_empty(cur) && !join_condition_check(&(cur->join.condition), cur)) {
            cursor_next(cur);
        }
    }
    return cur;
}

#ifndef LLP_TABLE_BUILDER_H
#define LLP_TABLE_BUILDER_H

#include "client_manager/database/database.h"

struct table_builder_column {
    char *column_name;

    enum data_type column_type;
    struct table_builder_column *next_col;
};

struct table_builder {
    char *table_name;
    struct table_builder_column *column_list;
    size_t column_num;
};

struct table_builder *table_builder_init(char *table_name);
void table_builder_free(struct table_builder **table);
void table_builder_add_column(struct table_builder *table, char *col_name, enum data_type col_type);

#endif

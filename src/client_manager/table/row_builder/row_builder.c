#include <assert.h>
#include "row_builder.h"
#include "stdlib.h"

struct row_builder row_builder_init(size_t capacity) { return (struct row_builder) {
            .size = 0,
            .capacity = capacity,
            .columns = malloc(sizeof(struct row_column) * capacity) };
}

void row_builder_free(struct row_builder *row_builder) {
    assert(row_builder != NULL);
    if (row_builder->columns == NULL) {
        return;
    }
    free(row_builder->columns);
    row_builder->columns = NULL;
}



struct row row_builder_as_row(struct row_builder *row_builder) {
    return (struct row) {
            .size = row_builder->size,
            .columns = row_builder->columns
    };
}

void row_builder_add(struct row_builder *row_builder, struct row_column col) {
    assert(row_builder != NULL);
    if (row_builder->size == row_builder->capacity) {
        return;
    }
    row_builder->columns[row_builder->size] = col;
    row_builder->size++;
}

struct batch_builder batch_builder_init(size_t capacity) {
    return (struct batch_builder) {
            .capacity = capacity,
            .size = 0,
            .rows = malloc(sizeof(struct row_builder) * capacity)
    };
}

void batch_builder_free(struct batch_builder *batch_builder) {
    assert(batch_builder != NULL);
    if (NULL == batch_builder->rows) {
        return;
    }
    for (size_t i = 0; i < batch_builder->size; i++) {
        row_builder_free(&batch_builder->rows[i]);
    }
    //printf()
    free(batch_builder->rows);
    batch_builder->rows = NULL;
}

void batch_builder_add(struct batch_builder *batch_builder, struct row_builder row) {
    assert(batch_builder != NULL);
    if (batch_builder->size == batch_builder->capacity) {
        return;
    }
    batch_builder->rows[batch_builder->size] = row;
    batch_builder->size++;
}

#ifndef LLP_ROW_BUILDER_H
#define LLP_ROW_BUILDER_H

#include <stddef.h>
#include "client_manager/table/table.h"

struct row_builder {
    struct row_column *columns;
    size_t size;
    size_t capacity;
};

struct batch_builder {
    struct row_builder *rows;
    size_t size;
    size_t capacity;
};

struct row_builder row_builder_init(size_t capacity);
void row_builder_free(struct row_builder *row_builder);
void row_builder_add(struct row_builder *row_builder, struct row_column col);
struct row row_builder_as_row(struct row_builder *row_builder);
struct batch_builder batch_builder_init(size_t capacity);
void batch_builder_free(struct batch_builder *batch_builder);
void batch_builder_add(struct batch_builder *batch_builder, struct row_builder row);

#endif

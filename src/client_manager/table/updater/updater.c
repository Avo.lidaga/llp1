#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "updater.h"

struct column_updater *column_updater_of(char *target, struct row_column new_value) {
    struct column_updater *updater = malloc(sizeof(struct column_updater));
    updater->new_value = new_value;
    updater->target.name = target;
    updater->translated = false;
    return updater;
}

struct updater_builder *updater_builder_init() {
    struct updater_builder *updater = malloc(sizeof(struct updater_builder));
    updater->list = NULL;
    return updater;
}

void updater_builder_free(struct updater_builder **updater_ptr) {
    assert(updater_ptr != NULL);
    struct updater_builder *updater = *updater_ptr;
    if (NULL == updater) {
        return;
    }
    struct column_updater_list *list = updater->list;
    while (list != NULL) {
        struct column_updater_list *next = list->next;
        free(list);
        list = next;
    }
    free(updater);
    *updater_ptr = NULL;
}

void updater_builder_add(struct updater_builder *updater, struct column_updater *col_updater) {
    struct column_updater_list *new_list = malloc(sizeof(struct column_updater_list));
    if (new_list != NULL) {
        new_list->updater = col_updater;
        new_list->next = NULL;
        struct column_updater_list *list = updater->list;
        if (list != NULL) {
            while (list->next != NULL) {
                list = list->next;
            }
            list->next = new_list;
        } else {
            updater->list = new_list;
        }
    }
}

struct row updater_builder_update(struct updater_builder *updater, struct row row) {
    struct row copy = row_copy(row);
    struct column_updater_list *list = updater->list;
    while (list != NULL) {
        struct column_updater_list *next = list->next;
        struct column_updater *col_updater = list->updater;
        assert(col_updater->translated);
        size_t column_idx = col_updater->target.idx;
        copy.columns[column_idx] = column_copy(col_updater->new_value);
        list = next;
    }
    return copy;
}

struct updater_builder *updater_builder_translate(struct updater_builder *old_updater, struct table_index_list *index_list) {
    struct updater_builder *new_updater = updater_builder_init();
    if (new_updater != NULL){
        struct column_updater_list *list = old_updater->list;
        while (list != NULL){
            struct column_updater_list *next = list->next;
            struct column_updater *col_updater = list->updater;
            struct column_index_list *column_list_inst = index_list->col_list;
            while (column_list_inst != NULL) {
                if (strcmp(column_list_inst->col_name, col_updater->target.name) == 0) {
                    unsigned int idx = column_list_inst->col_index;
                    struct column_updater *updater = malloc(sizeof(struct column_updater));
                    if (updater != NULL){
                        updater->new_value = col_updater->new_value;
                        updater->target.idx = idx;
                        updater->translated = true;
                        updater_builder_add(new_updater, updater);
                    }else{
                        updater_builder_free(&new_updater);
                        return NULL;
                    }
                }
                column_list_inst = column_list_inst->prev;
            }
            list = next;
        }
    }
    return new_updater;
}
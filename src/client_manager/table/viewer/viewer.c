#include <assert.h>
#include <stdlib.h>
#include "viewer.h"

void result_view_free(struct result_view **view_ptr) {
    assert(view_ptr != NULL);
    struct result_view *view = *view_ptr;
    if (NULL == view) {
        return;
    }
    free(view->view_selector);
    cursor_free(&(view->cursor));
    table_struct_free(&view->view_scheme);
    free(view);
    *view_ptr = NULL;
}

bool result_view_is_empty(struct result_view *view) {
    return cursor_is_empty(view->cursor);
}

struct row result_view_get(struct result_view *view) {
    size_t size = view->view_scheme->col_num;
    struct row result;
    result.size = size;
    result.columns = malloc(sizeof(struct row_column) * size);
    for (size_t i = 0; i < size; i++) {
        struct column_description description = view->view_selector[i];
        struct row_column col = cursor_get(view->cursor, description.index.table_idx, description.index.column_idx);
        result.columns[i] = col;
    }
    return result;
}

//rvn view->cursor
void result_view_next(struct result_view *view) {
    if (result_view_is_empty(view)) {
        return;
    }
    return cursor_next(view->cursor);
}

struct table_struct *result_view_scheme(struct result_view *view) {
    return view->view_scheme;
}
#ifndef LLP_CONDITION_H
#define LLP_CONDITION_H

#include <stdbool.h>
#include "client_manager/database/database.h"
#include "client_manager/table/table.h"

enum condition_type {
    CONDITION_AND = 0,
    CONDITION_OR = 1,
    CONDITION_NOT = 2,
    CONDITION_COMPARE = 3,
};

enum comparing_type {
    COMPARE_EQ = 0,
    COMPARE_NE = 1,
    COMPARE_GT = 2,
    COMPARE_LT = 3,
    COMPARE_GE = 4,
    COMPARE_LE = 5,
};

enum operand_value_type {
    OPERAND_VALUE_LITERAL = 0,
    OPERAND_VALUE_COLUMN = 1,
};

struct operand {
    enum operand_value_type type;
    union {
        struct row_column literal;
        struct column_description column;
    };
};

struct where_condition {
    enum condition_type type;
    union {
        struct {
            struct where_condition *first;
            struct where_condition *second;
        } and;
        struct {
            struct where_condition *first;
            struct where_condition *second;
        } or;
        struct {
            struct where_condition *first;
        } not;
        struct {
            enum comparing_type type;
            struct operand first;
            struct operand second;
        } compare;
    };
};

bool columns_equals(struct row_column first, struct row_column second);
void where_condition_free(struct where_condition *condition);
struct where_condition *where_condition_and(struct where_condition *first, struct where_condition *second);
struct where_condition *where_condition_compare(enum comparing_type type, struct operand op1, struct operand op2);
struct where_condition *where_condition_or(struct where_condition *first, struct where_condition *second);
struct where_condition *where_condition_not(struct where_condition *first);
struct operand operand_column(char *table, char *column);
struct operand operand_literal_float(float value);
struct operand operand_literal_int(int32_t value);
struct operand operand_literal_bool(bool value);
struct operand operand_literal_string(char *value);

#endif
